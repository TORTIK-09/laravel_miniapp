<x-guest-layout>
    <div style="color: var(--font-color);"  class="mb-4 text-sm text-gray-600 dark:text-gray-400">
        {{ __('Спасибо за регистрацию! Прежде чем начать, не могли бы вы подтвердить свой адрес электронной почты, нажав на ссылку, которую мы только что отправили вам по электронной почте? Если вы не получили письмо, мы с радостью отправим вам другое.') }}
    </div>
    @if (session('status') == 'verification-link-sent')
        <div style="color: var(--font-color);"  class="mb-4 font-medium text-sm text-green-600 dark:text-green-400">
            {{ __('Новая ссылка для подтверждения была отправлена на адрес электронной почты, который вы указали при регистрации.') }}
        </div>
    @endif
    <div class="mt-4 flex items-center justify-between">
        <form method="POST" action="{{ route('verification.send') }}">
            @csrf
            <div>
                <x-primary-button>
                    {{ __('Выслать сообщение повторно') }}
                </x-primary-button>
            </div>
        </form>
        <form method="POST" action="{{ route('logout') }}">
            @csrf
            <button style="color: var(--button-color);" type="submit" class="underline text-sm text-gray-600 dark:text-gray-400 hover:text-gray-900 dark:hover:text-gray-100 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 dark:focus:ring-offset-gray-800">
                {{ __('Выйти') }}
            </button>
        </form>
    </div>
    <br>
    <label style="margin-left:40%;" id="switch" class="switch">
        <input type="checkbox" onchange="toggleTheme()" id="slider">
        <span class="slider round"></span>
    </label>
</x-guest-layout>
