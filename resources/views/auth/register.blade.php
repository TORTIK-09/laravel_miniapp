<x-guest-layout>
    <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
        @csrf
        <!-- Name -->
        <div>
            <x-input-label style="color: var(--font-color);" for="name" :value="__('Ваше Имя')" />
            <x-text-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" />
            <x-input-error :messages="$errors->get('name')" class="mt-2" />
        </div>
        <!-- Email Address -->
        <div class="mt-4">
            <x-input-label style="color: var(--font-color);" for="email" :value="__('Ваша почта')" />
            <x-text-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autocomplete="username" />
            <x-input-error :messages="$errors->get('email')" class="mt-2" />
        </div>
        <x-input-label style="color: var(--font-color);" for="avatar" value="Ваш логотип" />
        <label class="block mt-2">
            <input style="background:white;color:black; border:solid; border-color: lightgray;border-radius: 10px; border-width: 1px;" type="file" id="avatar" name="avatar" class="block w-full text-sm text-slate-500
                                     mr-4 py-2 px-4
                                    text-sm font-semibold
                                    bg-violet-50 text-violet-700
                                    hover:bg-violet-100" accept="image/svg+xml, image/png, image/jpeg, image/jpg, image/webp"/>
        </label>
        <!-- Password -->
        <div class="mt-4">
            <x-input-label style="color: var(--font-color);" for="password" :value="__('Пароль')" />

            <x-text-input id="password" class="block mt-1 w-full"
                            type="password"
                            name="password"
                            required autocomplete="new-password" />

            <x-input-error :messages="$errors->get('password')" class="mt-2" />
        </div>
        <!-- Confirm Password -->
        <div class="mt-4">
            <x-input-label style="color: var(--font-color);" for="password_confirmation" :value="__('Подтвердите Пароль')" />

            <x-text-input id="password_confirmation" class="block mt-1 w-full"
                            type="password"
                            name="password_confirmation" required autocomplete="new-password" />

            <x-input-error :messages="$errors->get('password_confirmation')" class="mt-2" />
        </div>
        <div class="flex items-center justify-end mt-4">
            <a style="color: var(--font-color);" class="underline text-sm text-gray-600 dark:text-gray-400 hover:text-gray-900 dark:hover:text-gray-100 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 dark:focus:ring-offset-gray-800" href="{{ route('login') }}">
                {{ __('Есть Аккаунт?') }}
            </a>

            <x-primary-button class="ms-4">
                {{ __('Зарегистрироваться') }}
            </x-primary-button>
        </div>
    </form>
<br>
    <label style="margin-left:40%;" id="switch" class="switch">
        <input type="checkbox" onchange="toggleTheme()" id="slider">
        <span class="slider round"></span>
    </label>
</x-guest-layout>
