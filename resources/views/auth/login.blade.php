
<x-guest-layout >

    <!-- Session Status -->
    <x-auth-session-status class="mb-4" :status="session('status')" />

    <form style="" method="POST" action="{{ route('login') }}">
        @csrf

        <!-- Email Address -->
        <div>
            <x-input-label style="color: var(--font-color);" for="email" :value="__('Ваша почта')" />
            <x-text-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus autocomplete="username" />
            @error('email')
            <span class="" role="alert">
                                        <div style="color:red; font-size: 11px;">{{ 'Неверный логин-пароль или пользователь не существует' }}</div>
                                    </span>
            @enderror
        </div>

        <!-- Password -->
        <div class="mt-4">
            <x-input-label style="color: var(--font-color);" for="password" :value="__('Пароль')" />

            <x-text-input id="password" class="block mt-1 w-full"
                            type="password"
                            name="password"
                            required autocomplete="current-password" />

            @error('Password')
            <span class="" role="alert">
                                        <div style="color:red; font-size: 11px;">{{ 'Неверный логин-пароль или пользователь не существует' }}</div>
                                    </span>
            @enderror
        </div>

        <!-- Remember Me -->

        <div class="block mt-4">
            <label style="color: var(--font-color);" for="remember_me" class="inline-flex items-center">
                <input id="remember_me" type="checkbox" class="rounded dark:bg-gray-900 border-gray-300 dark:border-gray-700 text-indigo-600 shadow-sm focus:ring-indigo-500 dark:focus:ring-indigo-600 dark:focus:ring-offset-gray-800" name="remember">
                <span style="color: var(--font-color);" class="ms-2 text-sm text-gray-600 dark:text-gray-400">{{ __('Запомнить меня') }}</span>

            </label>

        </div>
            @if(Route::has('register'))
                <a style="color: var(--font-color);" class="flex-col  underline text-sm text-gray-600 dark:text-gray-400 hover:text-gray-900 dark:hover:text-gray-100 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 dark:focus:ring-offset-gray-800" href="{{ route('register') }}">
                    {{ __('Нет аккаунта?') }}
                </a>
            @endif


        <div class="flex items-center justify-end mt-4">

            @if (Route::has('password.request'))
                <div>
                <a style="color: var(--font-color);" class="underline text-sm text-gray-600 dark:text-gray-400 hover:text-gray-900 dark:hover:text-gray-100 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 dark:focus:ring-offset-gray-800" href="{{ route('password.request') }}">
                    {{ __(' Забыли пароль? ') }}
                </a>
            @endif
            <x-primary-button class="ms-4">
                {{ __('Войти') }}
            </x-primary-button>
        </div>

        </div>
        <br>
        <label style="margin-left:40%;" id="switch" class="switch">
            <input type="checkbox" onchange="toggleTheme()" id="slider">
            <span class="slider round"></span>
        </label>
    </form>

</x-guest-layout>
