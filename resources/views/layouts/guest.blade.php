<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'MiniApp') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="{{asset('css/theme.css')}}" type="text/css">
        <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}" type="image/x-icon">
        <!-- Scripts -->
        @vite(['resources/css/app.css', 'resources/js/app.js'])
    </head>
    <style>
        body{
            background: var(--color-secondary);
        }
    </style>
    <body class="font-sans antialiased">



        <div class="min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0 ">
            <div>
                <a href="/">
                    <x-application-logo style="height: 90px;" class="w-auto  fill-current text-gray-500" />
                </a>
            </div>

            <div style="background: var(--color-form); " class="w-full sm:max-w-md mt-6 px-6 py-4 bg-white dark:bg-gray-800 shadow-md overflow-hidden sm:rounded-lg">
                {{ $slot }}
            </div>
        </div>


    <script src="{{asset('js/theme.js')}}"></script>
    </body>
</html>
