<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name') }}</title>

        <!-- Fonts -->
        <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}" type="image/x-icon">
        <link rel="stylesheet" href="{{asset('css/theme.css')}}" type="text/css">
        <!-- Scripts -->

        @vite(['resources/css/app.css', 'resources/js/app.js'])
    </head>
    <style>
        html,
        body {
            margin: 0;
            padding: 0;
            height: 100%;
            width: 100%;
            display: flex;
            background: var(--color-secondary);
            color: var(--font-color);
            flex-direction: column;
        }
        @media screen and (max-width: 720px)
        {
            .hide1{
                display: block;
                background: white;
                color:white;
            }
        }
    </style>
    <body class="font-sans antialiased">
        <div class="min-h-screen ">
            @include('layouts.navigation')

            <!-- Page Heading -->
            @if (isset($header))
                <header style="background: var(--header-color); color: var(--headerfont-color);" class=" shadow">
                    <div  class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                        {{ $header }}
                    </div>
                </header>
            @endif

            <!-- Page Content -->
            <main>
                {{ $slot }}
            </main>
        </div>
        <script src="{{asset('js/theme.js')}}"></script>
        <script>
            document.getElementById("send_enter").addEventListener("keyup", function(e) {
                if (e.keyCode === 13) {
                    document.getElementById('send_enter').submit();
                    return false
                }
            });
        </script>
    </body>
</html>
