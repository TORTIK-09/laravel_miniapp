<section>
    <header>
        <h2 class="text-lg font-medium text-gray-900 dark:text-gray-100">
            {{ __('Информация о профиле') }}
        </h2>

        <p class="mt-1 text-sm text-gray-600 dark:text-gray-400">
            {{ __("Обновите информацию профиля вашей учетной записи и адрес электронной почты.") }}
        </p>
    </header>

    <form id="send-verification" method="post" action="{{ route('verification.send') }}">
        @csrf
    </form>

    <form method="post" action="{{ route('profile.update') }}" class="mt-6 space-y-6" enctype="multipart/form-data">
        @csrf
        @method('patch')

        <div style="color: var(--headerfont-color);">
            <x-input-label for="name" :value="__('Текущее имя')" />
            <x-text-input id="name" name="name" type="text" class="mt-1 block w-full" :value="old('name', $user->name)" required autofocus autocomplete="name" />
            <x-input-error class="mt-2" :messages="$errors->get('name')" />
        </div>

        <div style="color: var(--headerfont-color);">
            <x-input-label for="email" :value="__('Текущая почта')" />
            <x-text-input id="email" name="email" type="email" class="mt-1 block w-full" :value="old('email', $user->email)" required autocomplete="username" />
            <x-input-error class="mt-2" :messages="$errors->get('email')" />
            @if ($user instanceof \Illuminate\Contracts\Auth\MustVerifyEmail && ! $user->hasVerifiedEmail())
                <div>
                    <p style="color: var(--text-button);"  class="text-sm mt-2 text-gray-800 dark:text-gray-200">
                        {{ __('Ваш адрес электронной почты не подтвержден.') }}

                        <button style="color: var(--text-button);"  form="send-verification" class="underline text-sm text-gray-600 dark:text-gray-400 hover:text-gray-900 dark:hover:text-gray-100 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 dark:focus:ring-offset-gray-800">
                            {{ __('Нажмите здесь, чтобы повторно отправить письмо с подтверждением.') }}
                        </button>
                    </p>
                    @if (session('status') === 'verification-link-sent')
                        <p style="color: var(--text-button);"   class="mt-2 font-medium text-sm text-green-600 dark:text-green-400">
                            {{ __('На ваш адрес электронной почты была отправлена новая ссылка для подтверждения.') }}
                        </p>
                    @endif
                </div>
            @endif
        </div>
        <div class="mt-1 w-full">
            <x-input-label for="email" :value="__('Текущий логотип')" />
            <div style="border:solid; border-color: lightgray;border-bottom-width: 0px; border-top-left-radius: 10px;border-top-right-radius:10px; border-top-width: 1px; border-left-width: 1px; border-right-width: 1px;" >
            <img src="{{Storage::disk('avatars')->url(request()->user()->avatar)}}" width="96" height="96">
            </div>
            <input style="background:white;color:var(--headerfont-color); border:solid; border-color: lightgray; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px; border-width: 1px;"  type="file" value="{{old('avatar', $user->avatar)}}" id="avatar" name="avatar" class="block w-full text-sm text-slate-500
                                    mr-4 py-2 px-4
                                    text-sm font-semibold
                                    bg-violet-50 text-violet-700
                                    hover:bg-violet-100
                                " accept="image/svg+xml, image/png, image/jpeg, image/jpg, image/webp"/>
        <br>
            <div class="flex items-center gap-4">

            <x-primary-button>{{ __('Сохранить') }}</x-primary-button>

            @if (session('status') === 'profile-updated')
                <p
                    x-data="{ show: true }"
                    x-show="show"
                    x-transition
                    x-init="setTimeout(() => show = false, 2000)"
                    style="color: var(--text-button);"
                    class="text-sm text-gray-600 dark:text-gray-400"
                >{{ __('Сохранено.') }}</p>
            @endif
        </div>
    </form>
</section>
