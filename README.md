#Hello People, it's MiniApp!


#Базовые действия для запуска сайта

- .env.example -> .env - Настройте под себя и переименуйте;

- composer update - Загрузка зависимостей;

- php artisan key:generate - Создание уникального ключа приложения;

- php artisan migrate - Миграция в базу данных;

- php artisan storage:link - Создание пути для хранения логотипов;

- npm install - Загрузка frontend зависимостей;

- npm run build - Сборка frontend части проекта;

- php artisan serve - Запуск сервера.


#Полезные команды

- php artisan vendor:publish --tag=laravel-notifications - Выгрузка шаблона письма на почту.

- php artisan model:prune - Очистка писем старше указанного в функции срока(По умолчанию: 7 дней);
