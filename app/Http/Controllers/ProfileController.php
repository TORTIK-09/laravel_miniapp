<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileUpdateRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Auth\RegisteredUserController as avatar;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Illuminate\Support\Facades\File;
class ProfileController extends Controller
{
    /**
     * Показать пользовательскую форму обновления данных.
     */
    public function edit(Request $request): View
    {
        return view('profile.edit', [
            'user' => $request->user(),
        ]);
    }

    /**
     * Обновить информацию пользователя
     */



    public function update(ProfileUpdateRequest $request): RedirectResponse
    {
        $fileName = '';
        $request_avatar = request()->hasFile('avatar');
        $request->user()->fill($request->validated());
        
        if ($request->user()->isDirty('email')) {
            $request->user()->email_verified_at = null;
        }

        switch ($request_avatar) {
            case $request_avatar != " " :
                $fileName= request()->user()->avatar;
                if(request()->user()->avatar){
                    Storage::delete('public/avatar_images/' . $fileName);
                }
                break;
            default:
            $fileName = $request->user()->avatar;
                break;
        }

        if($request_avatar == " "){
            $fileName = request()->file('avatar')->store('', 'avatars');
        }

        if($request_avatar) {
            request()->avatar->storeAs('public/avatar_images', $fileName);
        }

        $request->user()->avatar = $fileName;
        $request->user()->save();
       
        return Redirect::route('profile.edit')->with('status', 'profile-updated');
    }
    /**
     * Удалить аккаунт пользователя.
     */
    public function destroy(Request $request): RedirectResponse
    {
        $request->validateWithBag('userDeletion', [
            'password' => ['required', 'current_password'],
        ]);

        $user = $request->user();

        Auth::logout();

        $user->delete();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return Redirect::to('/');
    }
}
