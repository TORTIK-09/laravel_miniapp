<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\MassPrunable;
class Chirp extends Model
{
    use HasFactory;
    use MassPrunable;

    public function prunable()
    {
     return static::where('created_at', '<=', now()->subDays(7)); #Очистка письма берет дату отправки из пункта 'created_at' в базе данных;
    }
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    protected $fillable = [
        'message',
    ];




}
