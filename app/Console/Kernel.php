<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     */
    protected function schedule(Schedule $schedule): void
    {
        // $schedule->command('inspire')->hourly();
        $schedule->command('model:prune')->twiceDaily(0, 13); #Выполнение автоматической очистки в 00:00 и 13:00
        #Так же если добавляем файлы в письмо меняем команду на
        #schedule->command('model:prune, ['--model' => [Chirp::class, Attachment::class],])->twiceDaily(0, 13);
    }
    /**
     * Register the commands for the application.
     */
    protected function commands(): void
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
